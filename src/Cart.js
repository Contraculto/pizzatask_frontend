/*
	The Pizza Tusk
	Main React Component
	Main structure and layout of the site, loads other component

	Rodrigo Lanas
	rodrigo@contraculto.com
*/

//	Requirements
import React from "react";
import { Table, Button } from 'reactstrap';

//	Component
const cart = (props) => {


	//  Helper variables
	var totalUsd = 0;
	var totalItems = 0;
	var check = null;

	//  Check if there are any products in the cart
	for ( var prop in props.cartProducts ) {
		if ( props.cartProducts.hasOwnProperty(prop) ) {
			if ( props.cartProducts[prop] > 0) {
				check = 1;
				break;
			}
		}
	}

	//  Render the cart
	if ( check === 1 ) {
		return (
			<div id="cart" className="active">
				<h3>Items in your cart</h3>
				<Table borderless>
					<thead>
						<tr>
							<th>#</th>
							<th>Product</th>
							<th>type</th>
							<th>Price USD</th>
							<th>Price EUR</th>
						</tr>
					</thead>
					<tbody>
						{props.products.map((item) => {
							if ( props.cartProducts[item.id] > 0 ) {
								let totalPriceUsd = item.price_usd * props.cartProducts[item.id];
								totalUsd = totalUsd + totalPriceUsd;
								totalItems = totalItems + props.cartProducts[item.id];
								return <tr key={item.id}>
									<td>{props.cartProducts[item.id]}</td>
									<td>{item.name}</td>
									<td>{item.type}</td>
									<td>${totalPriceUsd.toFixed(2)}</td>
									<td>€{(totalPriceUsd * props.euroValue).toFixed(2)}</td>
								</tr>
							}
							return false;
						})}
						<tr>
							<th>{totalItems}</th>
							<th></th>
							<th></th>
							<th>${totalUsd.toFixed(2)}</th>
							<th>€{(totalUsd * props.euroValue).toFixed(2)}</th>
						</tr>
					</tbody>
				</Table>
				<div className="totals">
					<p className="delivery">{props.deliveryCost ? "Delivery cost is $" + props.deliveryCost.toFixed(2) + " (€" + (props.deliveryCost * props.euroValue).toFixed(2) + ")" : "Select your district to estimate delivery cost"}</p>
					<p className="total">{props.deliveryCost ? "Your total is $" + (totalUsd + props.deliveryCost) + " (€" + ((totalUsd + props.deliveryCost) * props.euroValue).toFixed(2) + ")" : "Select your district to estimate total cost"}</p>
				</div>
				<div className="button">
					<Button onClick={props.showOrderBox}>I want this</Button>
				</div>
			</div>
		)
	} else {
		return <div id="cart">
			<p>Your cart is empty</p>
		</div>
	}

}
 
export default cart;