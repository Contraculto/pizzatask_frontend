/*
	The Pizza Tusk
	Product Component
	Renders a single product card for the website

	Rodrigo Lanas
	rodrigo@contraculto.com
*/

//	Requirements
import React from "react";
import { Col } from 'reactstrap';

//	Component
const product = (props) => {

	return (
	<Col md="3" xs="6" key={props.item.id}>
		<div className={"product" + (props.inCart > 0 ? " active" : "")} key={props.item.id} id={props.item.id}>
			<h3>{props.item.name}</h3>
			<div className="controls">
				<div className="overlay">
					<h4><a href="https://www.innoscripta.com/" onClick={(e) => { e.preventDefault(); console.log("-" + props.item.id); props.updateCart("substract", props.item.id); } } className="substract">-</a> <span className="amount">{ props.inCart ? props.inCart : 0 }</span> <a href="https://www.innoscripta.com/" onClick={(e) => { e.preventDefault(); console.log("-" + props.item.id); props.updateCart("add", props.item.id); } } className="add">+</a></h4>
				</div>
				<img src={process.env.PUBLIC_URL + "pizzas/" + props.item.picture} alt={props.item.name}></img>
			</div>
			<div className="details">
				<p>{props.item.description}</p>
				<p><strong>${props.item.price_usd}</strong> { props.item.vegetarian === "yes" ? "VEGETARIAN" : "" }</p>
			</div>
		</div>
	</Col>
	);

}
 
export default product;