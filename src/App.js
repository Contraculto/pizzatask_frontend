/*
	The Pizza Tusk
	Main React Component
	Main structure and layout of the site, loads other component

	Rodrigo Lanas
	rodrigo@contraculto.com
*/

//	Requirements
import React from 'react';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';

import './App.css';
import Cart from './Cart.js';
import Product from './Product.js';

//	Settings
var euroValue = 1.1;

//	Show the Order Box
function showOrderBox() {
	document.getElementById("order").style.display = "flex";
	document.getElementById("header").style.display = "none";
	document.getElementById("content").style.display = "none";
	document.getElementById("footer").style.display = "none";
}

//	Hide the Order Box
function hideOrderBox() {
	document.getElementById("order").style.display = "none";
	document.getElementById("header").style.display = "flex";
	document.getElementById("content").style.display = "flex";
	document.getElementById("footer").style.display = "flex";
}

// Main component
class App extends React.Component {

	//	Constructor for state
	constructor(props) {

		super(props);
		this.state = {
			products: [],
			cartProducts: {},
			deliveryCost: null
		};

	}

	//	Add or remove a product to/from the cart 
	updateCartProducts = (operation, id) => {
		var cart = this.state.cartProducts;
		if ( operation === "add" ) {
			if ( typeof cart[id] != "undefined" ) {
				++cart[id];
			} else {
				cart[id] = 1;
			}
		} else if ( operation === "substract" ) {
			if ( cart[id] > 0 ) {
				--cart[id];
			} else {
				delete cart[id];
			}
		}
		this.setState({cartProducts: cart});
	}

	//	Update the delivery cost based on city district
	updateDeliveryCost = (e) => {

		//	Different zones of the city have different delivery costs
		let zone1 = ["Admiralteysky", "Vasileostrovsky", "Vyborgsky", "Kalininsky", "Kirovsky", "Kolpinsky"];
		let zone2 = ["Krasnogvardeysky", "Krasnoselsky", "Kronshtadtsky", "Kurortny", "Moskovsky", "Nevsky"];
		let zone3 = ["Petrogradsky", "Petrodvortsovy", "Primorsky", "Pushkinsky", "Frunzensky", "Tsentralny"];

		//	Check and fill
		if ( zone1.includes(e.target.value) ) {
			this.setState({deliveryCost: 5});
		} else if ( zone2.includes(e.target.value) ) {
			this.setState({deliveryCost: 6});
		} else if ( zone3.includes(e.target.value) ) {
			this.setState({deliveryCost: 7});
		} else {
			this.setState({deliveryCost: null});
		}
	}

	//	Showtime
	render() {

		let mainContent = <Row id="content">
			<Col className="loading" md="12">
				<p>Loading products, give us a second...</p>
			</Col>
		</Row>
		if ( this.state.products.length > 0 ) {
			mainContent = <Row id="content">
				<Row className="section_title">
					<Col md="12">
						<h2>Our pizzas</h2>
					</Col>
				</Row>
				<Row className="section_content">
					{this.state.products.filter(e => e.type === "pizza").map((item) => {
						return <Product item={item} inCart={this.state.cartProducts[item.id]} updateCart={this.updateCartProducts} />
					})}
				</Row>

				<Row className="section_title">
					<Col md="12">
						<h2>Side dishes</h2>
					</Col>
				</Row>
				<Row className="section_content">
					{this.state.products.filter(e => e.type === "side").map((item) => {
						return <Product item={item} inCart={this.state.cartProducts[item.id]} updateCart={this.updateCartProducts} />
					})}
				</Row>

				<Row className="section_title">
					<Col md="12">
						<h2>Very refreshing drinks</h2>
					</Col>
				</Row>
				<Row className="section_content">
					{this.state.products.filter(e => e.type === "drink").map((item) => {
						return <Product item={item} inCart={this.state.cartProducts[item.id]} updateCart={this.updateCartProducts} />
					})}
				</Row>
			</Row>
		}

		return (
			<Container id="app">

				<Row id="order">
					<Col md="12">
						<h2>Please confirm your order to The Pizza Tusk</h2>
					</Col>
					<Col md="6">
						<Cart products={this.state.products} cartProducts={this.state.cartProducts} euroValue={euroValue} deliveryCost={this.state.deliveryCost} />

					</Col>
					<Col md="6">
						<h3>Delivery information</h3>
						<Form method="post">
							<FormGroup>
								<Label for="orderName">Name</Label>
								<Input type="text" name="name" id="orderName" placeholder="Enter your name" />
							</FormGroup>
							<FormGroup>
								<Label for="orderPhone">Telephone</Label>
								<Input type="text" name="phone" id="orderPhone" placeholder="Thelephone number" />
							</FormGroup>
							<FormGroup>
								<Label for="orderAddress">Address</Label>
								<Input type="text" name="address" id="orderAddress" placeholder="Your address" />
							</FormGroup>
							<FormGroup>
								<Label for="orderDistrict">District</Label>
								<Input type="select" onChange={this.updateDeliveryCost} name="district" id="orderDistrict">
									<option>-</option>
									<option>Admiralteysky</option>
									<option>Vasileostrovsky</option>
									<option>Vyborgsky</option>
									<option>Kalininsky</option>
									<option>Kirovsky</option>
									<option>Kolpinsky</option>
									<option>Krasnogvardeysky</option>
									<option>Krasnoselsky</option>
									<option>Kronshtadtsky</option>
									<option>Kurortny</option>
									<option>Moskovsky</option>
									<option>Nevsky</option>
									<option>Petrogradsky</option>
									<option>Petrodvortsovy</option>
									<option>Primorsky</option>
									<option>Pushkinsky</option>
									<option>Frunzensky</option>
									<option>Tsentralny</option>
								</Input>
							</FormGroup>
							<FormGroup>
								<Label for="orderSpecialInstructions">Special instructions</Label>
								<Input type="specialinstructions" name="specialinstructions" id="orderSpecialInstructions" placeholder="Anything you think we should know" />
							</FormGroup>
							<Button disabled={this.state.deliveryCost? "" : "disabled"}>Place order</Button>
							<Button color="cancel" onClick={hideOrderBox}>Cancel</Button>
						</Form>
					</Col>
				</Row>

				<Row id="header">
					<Col md="6">
						<img src={process.env.PUBLIC_URL + "LogoPizzaTusk2.svg"} height="200" alt="The Pizza Tusk"></img>
					</Col>
					<Col md="6" position="relative">
						<Cart showOrderBox={showOrderBox} products={this.state.products} cartProducts={this.state.cartProducts} euroValue={euroValue} deliveryCost={this.state.deliveryCost} />
					</Col>
				</Row>

				{mainContent}

				<Row id="footer">
					<Col md="12">
						<p>This is a <strong><a href="https://www.innoscripta.com/">Pizza Siblings And Extended Family International™</a></strong> venture</p>
						<p><a href="https://www.innoscripta.com/">About us</a> | <a href="https://www.innoscripta.com/">Privacy Policy</a> | <a href="https://www.innoscripta.com/">Contact</a> | <a href="https://www.innoscripta.com/">Jobs</a></p>
					</Col>
				</Row>

			</Container>
		);
	}

	//	Fetch the products from the backend
	componentDidMount() {
		fetch("http://contraculto.com:3001/api/products")
		.then(res => res.json())
		.then(data => this.setState({'products': data}));
	}
}

export default App;
