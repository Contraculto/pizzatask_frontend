## The Pizza Tusk

This repository contains the Customer-facing website for The Pizza Tusk, a fictional pizza parlor made by Rodrigo Lanas for The Pizza Task.

To run the project it's necesarry to "npm install" the dependencies and the run it with "npm start".

It's a simple React App with a couple of components. It uses react-async for loading the products and reactstrap for most CSS tyles.

The products are loaded from the Laravel backend using a simple (unauthenticated) API call.

The process works up to the "Confirm order" button which just posts to itself.

The Euro Value and Delivery Zones are configured inside the main component. They could have been loaded from the backend (for example) but I felt that was outside the scope of this project.